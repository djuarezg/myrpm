Name: myrpm
Version: 1.1
Release: 8%{?dist}
Summary: Super useful program

Group: CERN/Utilities
License: CERN
URL: 	https://cern.ch/myrpm
Source: %{name}-%{version}.tgz

BuildRequires: gcc-c++

%define debug_package %{nil}


%description
Just try to live without this

%prep
%setup -n %{name}-%{version}


%build
rm -rf %{buildroot}
cd src/
g++ -Wall -o hello-world hello-world.cpp


%install
install -d %{buildroot}%{_bindir}
cd src/
install -m 0755 hello-world %{buildroot}%{_bindir}/hello-world


%files
%defattr(-,root,root,-)
%{_bindir}/hello-world
%doc src/README.md



%changelog
* Mon Jan 13 2020 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.1-3
- Version bump

* Mon Nov 25 2019 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.1-2
- Version bump

* Fri Oct 11 2019 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.0-1
- Initial version
